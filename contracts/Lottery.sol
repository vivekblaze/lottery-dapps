// SPDX-License-Identifier: MIT

pragma solidity >0.5.0 <0.9.0;

contract Lottery {
    //Lottery owner address
    address payable lotteryOwner;
    
    //Participants
    address payable [] lotteryPlayers;

    //Lotery ID counter
    uint8 lotteryIDCounter;

    //status of lottery
    enum status {
        NotStarted,
        Open,
        Closed,
        Completed
    }

    //Lottery Winner
    struct winner {
        address winnerAddress;
        uint256 winnerReward;
    }       
    
    //Lotter Information
    struct lotteryInfo {
        uint256 lotteryID;        
        status lotteryStatus;
        uint256 costPerTicket;
        uint256 drawMinFund;
        uint256 lotteryDuration;
        uint256 startingTimestamp;
        uint256 endingTimestamp;
        uint256 rewardPercentage;
        uint256 playersCount;
        uint256 currentBalance;        
        winner lotteryWinner; 
    }

    //All Lotteries list (history)
    mapping(uint256 => lotteryInfo) internal allLotteries;

    event LotteryOpen(uint256 lotteryId);

    modifier onlyLotteryOwner {
        require(msg.sender == lotteryOwner,"Access Denied!");
        _;
    }

    constructor() {
        //Defaults
        lotteryOwner = payable(msg.sender);     
    }

    function add(uint8 a, uint8 b) internal pure returns (uint8) {
        uint8 c = a + b;
        require(c >= a, "addition overflow");
        return c;
    }

    function buyTicket() public payable {

        require(allLotteries[lotteryIDCounter].lotteryStatus == status.Open,"Sorry the lottery is not open now");
        require(block.timestamp >= allLotteries[lotteryIDCounter].startingTimestamp && block.timestamp < allLotteries[lotteryIDCounter].endingTimestamp,"Sorry, incorrect time to enter into the lottery");
        require(msg.value == allLotteries[lotteryIDCounter].costPerTicket,"Please send valid cost to enter into the lottery");

        //Add the buyer in the lottery
        lotteryPlayers.push(payable(msg.sender));
        
        allLotteries[lotteryIDCounter].playersCount = lotteryPlayers.length;
        allLotteries[lotteryIDCounter].currentBalance = address(this).balance;        
    }

    function createLottery(uint256 costPerTicket_,uint drawMinFund_,uint lotteryDuration_,uint rewardPercentage_) external onlyLotteryOwner() returns(uint256 lotteryID) {

        require(allLotteries[lotteryIDCounter].lotteryStatus != status.Open,"Unable to create new lottery when another lottery is open");
        
        lotteryIDCounter = add(lotteryIDCounter,1);
        lotteryID = lotteryIDCounter;
        uint256 startingTimestamp_ = block.timestamp;
        uint256 endingTimestamp_ = startingTimestamp_ + lotteryDuration_ * 1 minutes;        
        status lotteryStatus_ = status.Open;

        //Create and start a new lottery
        lotteryInfo storage newLottery = allLotteries[lotteryIDCounter];
        newLottery.lotteryID = lotteryID;
        newLottery.lotteryStatus = lotteryStatus_;
        newLottery.costPerTicket = costPerTicket_ * 1 ether;
        newLottery.drawMinFund = drawMinFund_;
        newLottery.lotteryDuration = lotteryDuration_;
        newLottery.startingTimestamp = startingTimestamp_;
        newLottery.endingTimestamp = endingTimestamp_;
        newLottery.rewardPercentage = rewardPercentage_;        
        
        emit LotteryOpen(lotteryID);
    }

    function draw() public payable onlyLotteryOwner {
        require(lotteryPlayers.length != 0,"No players found in the lottery");
        require(allLotteries[lotteryIDCounter].endingTimestamp <= block.timestamp, "Sorry, can not set winner during lottery");
        require(address(this).balance >= allLotteries[lotteryIDCounter].drawMinFund * 1 ether, "Minimum fund not reached to draw the winner");

        //Find winner
        bytes memory randomInfo = abi.encodePacked(block.timestamp,block.difficulty,lotteryPlayers.length);
        bytes32 randomHash = keccak256(randomInfo);
        uint256 winnerIndex = uint(randomHash)%lotteryPlayers.length;
        address payable lotteryWinner_ = payable(lotteryPlayers[winnerIndex]);        

        //Winner Reward
        uint256 rewardPercentage_ = allLotteries[lotteryIDCounter].rewardPercentage;
        uint256 winnerReward_ = address(this).balance * rewardPercentage_ / 100;

        //transfer reward to winner
        lotteryWinner_.transfer(winnerReward_);

        //transfer balance to owner
        lotteryOwner.transfer(address(this).balance);

        //Store winner details in lottery        
        allLotteries[lotteryIDCounter].lotteryWinner.winnerAddress = lotteryWinner_; 
        allLotteries[lotteryIDCounter].lotteryWinner.winnerReward = winnerReward_;
        allLotteries[lotteryIDCounter].lotteryStatus = status.Completed;

        delete lotteryPlayers;
    }

    function refund() public payable onlyLotteryOwner {
        require(allLotteries[lotteryIDCounter].endingTimestamp <= block.timestamp, "Sorry, can not refund during lottery");

        if(lotteryPlayers.length > 0 && address(this).balance > 0) {
            uint256 lotteryPlayersLen_ = lotteryPlayers.length;
            for(uint i = 0; i<lotteryPlayersLen_;i++){
                lotteryPlayers[i].transfer(allLotteries[lotteryIDCounter].costPerTicket);
            }
        }             
        allLotteries[lotteryIDCounter].lotteryStatus = status.Closed;

        delete lotteryPlayers;
    }

    function getContractBalance() public view returns(uint) {
        return address(this).balance;
    }

    function getPlayers() public view returns(address payable [] memory) {
        return lotteryPlayers;
    }

    function getLotteryInfo() external view returns(lotteryInfo memory) {
        return allLotteries[lotteryIDCounter];
    }

    function getAllLotteriesLength() external view returns(uint8) {
        return lotteryIDCounter;
    }

    function getLotteryByIndex(uint8 index) external view returns(lotteryInfo memory) {
        return allLotteries[index];
    } 
}