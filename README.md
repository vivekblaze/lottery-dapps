# Lottery DApps with web3 react

Lottery DApp is a decentralized application whose fundamental aspect is its Lottery Smart Contract which is deployed on the Ethereum blockchain and is responsible for conducting lotteries in a fair manner. The frontend for the DApp is built using Rea