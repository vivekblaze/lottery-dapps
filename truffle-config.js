const path = require("path");
require("dotenv").config({ path: "./.env" });
const HDWalletProvider = require("@truffle/hdwallet-provider");
const mnemonic = "";
const accountIndex = 0;

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    development: {
      port: 9545,
      network_id: "1337",
      host: "127.0.0.1",
    },
    ganachelocal: {
      provider: function () {
        return new HDWalletProvider(
          process.env.MNEMONIC,
          "http://127.0.0.1:7545",
          accountIndex
        );
      },
      network_id: "5777",
    },
    ropsten: {
      provider: function () {
        return new HDWalletProvider(
          process.env.MNEMONIC,
          "wss://ropsten.infura.io/ws/v3/9aa3d95b3bc440fa88ea12eaa4456161",
          accountIndex
        );
      },
      network_id: "3",
      gas: 4000000,
    },
  },
  compilers: {
    solc: {
      version: ">0.5.0 <0.9.0",
    },
  },
};
