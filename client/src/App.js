// ReactJS components
import React, { useState, useEffect } from "react";

// Ethereum contracts
import LOTTERYContract from "./contracts/Lottery.json";
// import getWeb3 from "./getWeb3";

// Local ReactJs components
import HomepageScreen from "./screens/HomepageScreen";
import LoginScreen from "./screens/LoginScreen";
import AdminDashboardScreen from "./screens/AdminDashboardScreen";
import LotteriesScreen from "./screens/LotteriesScreen";
import NotFound from "./screens/NotFound";
import ContractContex from "./context/ContractContex";

// React Router
import { Route, BrowserRouter as Router, Redirect } from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import { AuthProvider } from "./context/AuthContext";

import "./App.css";
import Web3 from "web3";
import { Button } from "@material-ui/core";

const App = () => {
  const [refresh, setrefresh] = useState(0);
  const [loading, setLoading] = useState(true);
  let content;
  const [web3Loaded, setWeb3Loaded] = useState(false);
  const [lotteryContract, setLotteryContract] = useState(null);
  const [web3, setWeb3] = useState(null);
  const [accounts, setAccounts] = useState(null);

  const loadWeb3 = async () => {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum);
      await window.ethereum.on("accountsChanged", (accounts) => {
        setAccounts(accounts);
      });
      await window.ethereum.request({ method: "eth_requestAccounts" });
    } else {
      window.alert(
        "Non-Ethereum browser detected. You should consider trying MetaMask!"
      );
    }
  };

  const loadBlockchainData = async () => {
    setLoading(true);
    if (typeof window.ethereum == "undefined") {
      return;
    }
    const web3 = new Web3(window.ethereum);

    const account = await web3.eth.getAccounts();

    if (account.length == 0) {
      return;
    }

    const networkId = await web3.eth.net.getId();
    if (networkId != "") {
      const lotteryContract = new web3.eth.Contract(
        LOTTERYContract.abi,
        LOTTERYContract.networks[networkId] &&
          LOTTERYContract.networks[networkId].address
      );

      setWeb3Loaded(true);
      setWeb3(web3);
      setLotteryContract(lotteryContract);
      setAccounts(account);
      setLoading(false);
    } else {
      window.alert("the contract not deployed to detected network.");
    }
  };

  const walletAddress = async () => {
    await window.ethereum.request({
      method: "eth_requestAccounts",
      params: [
        {
          eth_accounts: {},
        },
      ],
    });
    window.location.reload();
  };

  useEffect(() => {
    loadWeb3();
    loadBlockchainData();

    if (refresh == 1) {
      setrefresh(0);
      loadBlockchainData();
    }
    //esl
  }, [refresh]);

  return (
    <div>
      {accounts == "" && (
        <Button
          style={{ marginTop: 10 }}
          variant="outlined"
          color="default"
          onClick={walletAddress}
        >
          Connect MetaMask Wallet
        </Button>
      )}

      <AuthProvider>
        <Router>
          <div className="App">
            {!web3Loaded && <Redirect push to="/notfound" />}
            <ContractContex.Provider
              value={{ web3, accounts, lotteryContract }}
            >
              <Route path="/" exact component={HomepageScreen} />
              <Route path="/login" component={LoginScreen} />
              <Route exact path="/notfound" component={NotFound} />
              <PrivateRoute path="/admin" component={AdminDashboardScreen} />
              <PrivateRoute path="/lotteries" component={LotteriesScreen} />
            </ContractContex.Provider>
          </div>
        </Router>
      </AuthProvider>
    </div>
  );
};

export default App;
