import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyAPNY324THrQStrcwsSa5ud09S6SRHMuLM",
  authDomain: "eth-ico.firebaseapp.com",
  projectId: "eth-ico",
  storageBucket: "eth-ico.appspot.com",
  messagingSenderId: "839330208596",
  appId: "1:839330208596:web:f7ba219e343c0f521586c1",
};

const app = firebase.initializeApp(firebaseConfig);

export default app;
