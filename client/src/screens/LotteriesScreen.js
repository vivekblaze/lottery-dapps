// ReactJS imports
import React from "react";
// Local ReactJs components
import AdminHeader from "../components/AdminHeader";
// Contract context
import ContractContex from "../context/ContractContex";
import AllLotteries from "../components/AllLotteries";

export default function LotteriesScreen() {
  return (
    <ContractContex.Consumer>
      {(web3) => (
        <div>
          <AdminHeader contractContext={web3} />
          <AllLotteries contractContext={web3} />
        </div>
      )}
    </ContractContex.Consumer>
  );
}
