import React from "react";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  text: {
    color: "white",
    fontFamily: "monospace",
    fontWeight: "bold",
  },
});
const NotFound = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h1" component="h2" gutterBottom>
        MetaMask is logged out
      </Typography>
      <Link to="/" className={classes.text}>
        Go Home
      </Link>
    </div>
  );
};

export default NotFound;
