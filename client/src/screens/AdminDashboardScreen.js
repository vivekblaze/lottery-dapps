// ReactJS components
import React from "react";
// Local ReactJs components
import AdminHeader from "../components/AdminHeader";
import AdminContracts from "../components/AdminContracts";
// Contract context
import ContractContex from "../context/ContractContex";

export default function AdminDashboardScreen() {
  return (
    <ContractContex.Consumer>
      {(web3) => (
        <div>
          <AdminHeader contractContext={web3} />
          <AdminContracts contractContext={web3} />
        </div>
      )}
    </ContractContex.Consumer>
  );
}
