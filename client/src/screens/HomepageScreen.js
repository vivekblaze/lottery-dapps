// ReactJS components
import React from "react";

// Local ReactJs components
import HomepageHeader from "../components/HomepageHeader";
import HomepageInformation from "../components/HomepageInformation";
import LandingPageForm from "../components/LandingPageForm";
import ContractContex from "../context/ContractContex";

export default function HomepageScreen() {
  return (
    <ContractContex.Consumer>
      {(web3) => (
        <>
          <HomepageHeader contractContext={web3} />
          <HomepageInformation />
          <LandingPageForm contractContext={web3} />
        </>
      )}
    </ContractContex.Consumer>
  );
}
