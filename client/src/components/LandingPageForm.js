import React, { useState, useEffect } from "react";

// MaterialUI components
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Divider from "@material-ui/core/Divider";
import Countdown from "react-countdown";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import LocalOfferOutlinedIcon from "@material-ui/icons/LocalOfferOutlined";
import AccountCircleOutlinedIcon from "@material-ui/icons/AccountCircleOutlined";
import WorkIcon from "@material-ui/icons/Work";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: "3%",
    width: "40%",
  },
  rootList: {
    backgroundColor: theme.palette.background.paper,
    marginTop: 10,
    borderRadius: 15,
    borderColor: "#18FF1E",
    padding: 10,
  },
  root: {
    backgroundColor: "#20232a",
    border: "1px solid",
    borderRadius: 15,
    borderColor: "#18FF1E",
    padding: 10,
  },
  title: {
    color: "white",
    fontFamily: "monospace",
    fontWeight: "lighter",
  },
  button: {
    color: "white",
    fontSize: "15px",
    fontFamily: "monospace",
    fontWeight: "lighter",
    "&:hover": {
      backgroundColor: "#282c34",
    },
  },
  text: {
    color: "white",
    fontFamily: "monospace",
    fontWeight: "bold",
  },
  input: {
    color: "white",
  },
}));

const STATUS = ["NotStarted", "Open", "Closed", "Completed"];

export default function LandingPageForm(props) {
  const classes = useStyles();
  const [buyTicketAmount, setBuyTicketAmount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [toastMessage, setToastMessage] = useState({
    message: "",
    status: "success",
  });
  const [redirectPage, setRedirectPage] = useState(null);
  const [informations, setInformations] = useState({
    lotteryID: "0",
    lotteryStatus: STATUS[0],
    costPerTicket: "0",
    drawMinFund: "0",
    lotteryDuration: "0",
    startingTimestamp: "0",
    endingTimestamp: "0",
    rewardPercentage: "0",
    lotteryWinner: [],
    playersCount: "0",
    currentBalance: "0",
    winnerReward: 0,
  });

  const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  useEffect(() => {
    console.log("propspropsprops", props.contractContext);
    async function getLotteryInformations() {
      setLoading(true);
      let getLotteryInfo = await props.contractContext.lotteryContract.methods
        .getLotteryInfo()
        .call();
      console.log("getLotteryInfo", getLotteryInfo);
      setInformations({
        lotteryID: getLotteryInfo.lotteryID,
        lotteryStatus: STATUS[getLotteryInfo.lotteryStatus],
        costPerTicket: getLotteryInfo.costPerTicket,
        drawMinFund: getLotteryInfo.drawMinFund,
        lotteryDuration: getLotteryInfo.lotteryDuration,
        startingTimestamp: getLotteryInfo.startingTimestamp,
        endingTimestamp: getLotteryInfo.endingTimestamp,
        rewardPercentage: getLotteryInfo.rewardPercentage,
        lotteryWinner: getLotteryInfo.lotteryWinner,
        playersCount: getLotteryInfo.playersCount,
        currentBalance: getLotteryInfo.currentBalance,
        winnerReward:
          (parseFloat(
            props.contractContext.web3.utils.fromWei(
              getLotteryInfo.currentBalance.toString(),
              "ether"
            )
          ) /
            parseFloat(getLotteryInfo.rewardPercentage)) *
          100,
      });
      setLoading(false);
    }
    props.contractContext &&
      props.contractContext.lotteryContract &&
      getLotteryInformations();
  }, [open]);

  function handleChange(event) {
    setBuyTicketAmount(event.target.value);
  }

  // Handling the process of buying the tokens (interraction with kryptoniteTokenSale smart contract)
  async function buyTickets() {
    const ticketButAmt = props.contractContext.web3.utils.toWei(
      buyTicketAmount,
      "ether"
    );
    console.log("buyTicketAmount", ticketButAmt);
    setLoading(true);
    await props.contractContext.lotteryContract.methods
      .buyTicket()
      .send({
        from: props.contractContext.accounts[0],
        value: ticketButAmt,
      })
      .on("transactionHash", function (hash) {
        console.log("hash", hash);
      })
      .on("confirmation", function (confirmationNumber, receipt) {
        console.log("confirmationNumber", confirmationNumber);
      })
      .on("receipt", function (receipt) {
        console.log("receipt11111", receipt);
        setLoading(false);
        setToastMessage({
          message: "Ticket bought successully",
          status: "success",
        });
        setOpen(true);
      })
      .on("error", function (error, receipt) {
        console.log("error", error);
        setLoading(false);
        setToastMessage({
          message: "Unable to buy a ticket now",
          status: "error",
        });
        setOpen(true);
      });
  }

  const Informations = () => {
    const classes = useStyles();
    return (
      <List className={classes.rootList}>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <LocalOfferOutlinedIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Cost per ticket"
            secondary={
              props.contractContext.web3.utils.fromWei(
                informations.costPerTicket,
                "ether"
              ) + " ETH"
            }
          />
        </ListItem>
        <Divider variant="inset" component="li" />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <WorkIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Minimum draw fund"
            secondary={informations.drawMinFund + " ETH"}
          />
        </ListItem>
        <Divider variant="inset" component="li" />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <AccountCircleOutlinedIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Total Players"
            secondary={informations.playersCount}
          />
        </ListItem>
      </List>
    );
  };

  const renderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
      return <Completionist />;
    } else {
      return (
        <div>
          <Grid item xs={12}>
            <h1 className={classes.title}>Buy lottery tickets!</h1>
          </Grid>
          <div className="countdown-wrapper">
            <div className="time-section">
              <div className="countdown-value">{days}</div>
              <div className="countdown-unit">Days</div>
            </div>
            <div className="time-section">
              <div className="countdown-value">{hours}</div>
              <div className="countdown-unit">Hours</div>
            </div>
            <div className="time-section">
              <div className="countdown-value">{minutes}</div>
              <div className="countdown-unit">Mins</div>
            </div>
            <div className="time-section">
              <div className="countdown-value">{seconds}</div>
              <div className="countdown-unit">Secs</div>
            </div>
          </div>
          <Grid item xs={12}>
            <TextField
              className={classes.button}
              InputProps={{
                className: classes.input,
              }}
              id="ether"
              label="ETH Amount"
              variant="outlined"
              placeholder="ETH Amount"
              value={buyTicketAmount}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              className={classes.button}
              variant="outlined"
              onClick={buyTickets}
            >
              Buy ticket
            </Button>
          </Grid>
          <Informations />
        </div>
      );
    }
  };

  const Completionist = () => {
    const classes = useStyles();
    return (
      <div>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            {props.contractContext.web3.utils.fromWei(
              informations.currentBalance.toString(),
              "ether"
            ) >= informations.drawMinFund ? (
              <div>
                {informations.lotteryStatus == "Completed" ? (
                  <h1 className={classes.title}>
                    {`Winner is ${informations.lotteryWinner[
                      "winnerAddress"
                    ].slice(0, 6)}...${informations.lotteryWinner[
                      "winnerAddress"
                    ].slice(
                      informations.lotteryWinner["winnerAddress"].length - 4,
                      informations.lotteryWinner["winnerAddress"].length
                    )}`}
                  </h1>
                ) : (
                  <h1 className={classes.title}>
                    Winner will be announce in few moments!
                  </h1>
                )}
              </div>
            ) : (
              <div>
                {informations.lotteryStatus == "Closed" ? (
                  <h1 className={classes.title}>
                    We refunded the paymetns to your wallet
                  </h1>
                ) : (
                  <h1 className={classes.title}>
                    Sorry minimum fund not reached. we will refund shortly
                  </h1>
                )}
              </div>
            )}
          </Grid>
        </Grid>
      </div>
    );
  };

  if (redirectPage != null) {
    return redirectPage;
  } else {
    return (
      <Container className={classes.container}>
        <Paper className={classes.root} elevation={3}>
          <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity={toastMessage.status}>
              {toastMessage.message}
            </Alert>
          </Snackbar>

          <Grid container spacing={2}>
            {loading ? (
              <Grid item xs={12}>
                <CircularProgress color="secondary" />
              </Grid>
            ) : (
              <>
                {informations.lotteryID != 0 ? (
                  <Grid item xs={12}>
                    <Countdown
                      date={new Date(informations.endingTimestamp * 1000)}
                      renderer={renderer}
                    />
                    <Divider variant="inset" />
                  </Grid>
                ) : (
                  <Grid item xs={12}>
                    <h1 className={classes.title}>
                      Lottrery not created yet...
                    </h1>
                  </Grid>
                )}
              </>
            )}
          </Grid>
        </Paper>
      </Container>
    );
  }
}
