import React, { useState, useEffect } from "react";

// MaterialUI imports
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const useStyles = makeStyles({
  container: {
    marginTop: "3%",
    width: "60%",
  },
  table: {
    minWidth: 450,
    backgroundColor: "#20232a",
    color: "white",
  },
  tableContainer: {
    marginTop: 20,
    height: 450,
    backgroundColor: "#20232a",
    color: "white",
    border: "1px solid",
    borderRadius: 5,
  },
  text: {
    color: "white",
  },
  textCenter: {
    justifyContent: "center",
    textAlign: "center",
    color: "white",
  },
  title: {
    color: "white",
    fontFamily: "monospace",
    fontWeight: "lighter",
  },
});

const STATUS = ["NotStarted", "Open", "Closed", "Completed"];
export default function AllLotteries(props) {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [requests, setRequests] = useState([]);

  let tableRows = createRows(requests, classes);

  function createRows(requests, classes) {
    console.log("requests", requests.length);
    console.log("requests", JSON.stringify(requests));
    let tableRows = [];
    if (requests.length > 0) {
      requests.map((value, index) => {
        let startDate = new Date(requests[index].startingTimestamp * 1000);
        let endDate = new Date(requests[index].endingTimestamp * 1000);

        tableRows.push(
          <TableRow key={index}>
            <TableCell className={classes.text} align="center" scope="row">
              {index + 1}
            </TableCell>
            <TableCell className={classes.text} align="center" scope="row">
              {startDate.getFullYear() +
                "-" +
                (startDate.getMonth() + 1) +
                "-" +
                startDate.getDate()}
            </TableCell>
            <TableCell className={classes.text} align="center" scope="row">
              {endDate.getFullYear() +
                "-" +
                (endDate.getMonth() + 1) +
                "-" +
                endDate.getDate()}
            </TableCell>
            <TableCell className={classes.text} align="center" scope="row">
              {requests[index].lotteryStatus == "3"
                ? `${requests[index].lotteryWinner["winnerAddress"].slice(
                    0,
                    6
                  )}...${requests[index].lotteryWinner["winnerAddress"].slice(
                    requests[index].lotteryWinner["winnerAddress"].length - 4,
                    requests[index].lotteryWinner["winnerAddress"].length
                  )}`
                : "None"}
            </TableCell>
            <TableCell className={classes.text} align="center" scope="row">
              {requests[index].lotteryStatus == "3"
                ? props.contractContext.web3.utils.fromWei(
                    requests[index].lotteryWinner["winnerReward"],
                    "ether"
                  ) + " ETH"
                : "None"}
            </TableCell>
            <TableCell className={classes.text} align="center" scope="row">
              {STATUS[requests[index].lotteryStatus]}
            </TableCell>
          </TableRow>
        );
      });
    } else {
      tableRows.push(
        <TableRow key={0}>
          <TableCell className={classes.text} align="center">
            No lotteries available...
          </TableCell>
        </TableRow>
      );
    }
    return tableRows;
  }

  useEffect(() => {
    async function getRequests() {
      let getLotteryLength = await props.contractContext.lotteryContract.methods
        .getAllLotteriesLength()
        .call();

      if (Number(getLotteryLength) > 0) {
        let lotteryDetails = [];
        for (let i = 1; i <= getLotteryLength; i++) {
          let getLotteryInfo =
            await props.contractContext.lotteryContract.methods
              .getLotteryByIndex(i)
              .call();
          console.log("getLotteryInfo--- index mulitple", getLotteryInfo);
          lotteryDetails.push(getLotteryInfo);
        }
        setRequests(lotteryDetails);
      } else {
        setRequests([]);
      }

      setLoading(false);
    }
    getRequests();
  }, []);

  return (
    <Container className={classes.container}>
      <h1 className={classes.title}>Previous Lotteries</h1>
      {!loading && (
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table
            className={classes.table}
            size="small"
            aria-label="simple table"
          >
            <TableHead>
              <TableRow>
                <TableCell
                  className={classes.text}
                  align="center"
                  style={{ fontWeight: "bold" }}
                >
                  S.No
                </TableCell>
                <TableCell
                  className={classes.text}
                  align="center"
                  style={{ fontWeight: "bold" }}
                >
                  Start Time
                </TableCell>
                <TableCell
                  className={classes.text}
                  align="center"
                  style={{ fontWeight: "bold" }}
                >
                  End Time
                </TableCell>
                <TableCell
                  className={classes.text}
                  align="center"
                  style={{ fontWeight: "bold" }}
                >
                  Winner
                </TableCell>
                <TableCell
                  className={classes.text}
                  align="center"
                  style={{ fontWeight: "bold" }}
                >
                  Reward
                </TableCell>
                <TableCell
                  className={classes.text}
                  align="center"
                  style={{ fontWeight: "bold" }}
                >
                  Status
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{tableRows}</TableBody>
          </Table>
        </TableContainer>
      )}
    </Container>
  );
}
