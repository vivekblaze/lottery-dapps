import React, { useState } from "react";

// MaterialUI imports
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import ReactLogo from "../Logo.svg";
// React Router
import { Link, Redirect } from "react-router-dom";

// Firebase
import app from "../firebase/firebase";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    background: "#20232a",
  },
  menuButton: {
    marginRight: theme.spacing(2),
    "&:hover": {
      backgroundColor: "#282c34",
    },
  },
  title: {
    flexGrow: 1,
  },
  right: {
    marginLeft: "auto",
    "&:hover": {
      backgroundColor: "#282c34",
    },
  },
}));

export default function AdminHeader(props) {
  console.log(props);
  const classes = useStyles();
  const [redirectPage, setRedirectPage] = useState(null);

  function logout() {
    app.auth().signOut();
    setRedirectPage(<Redirect push to="/" />);
  }

  function prevLotteries() {
    setRedirectPage(<Redirect push to="/lotteries" />);
  }

  if (redirectPage != null) {
    return redirectPage;
  } else {
    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <Link to="/">
              <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="menu"
              >
                <img
                  src={ReactLogo}
                  alt="React Logo"
                  style={{ height: "60px", width: "60px" }}
                />
              </IconButton>
            </Link>
            <Typography variant="h6">
              {props.contractContext.accounts}
            </Typography>

            <Button
              color="inherit"
              className={classes.right}
              onClick={() => setRedirectPage(<Redirect push to="/admin" />)}
            >
              Admin Home
            </Button>

            <Button
              color="inherit"
              className={classes.button}
              onClick={prevLotteries}
            >
              Lotteries
            </Button>
            <Button color="inherit" className={classes.button} onClick={logout}>
              Logout
            </Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
