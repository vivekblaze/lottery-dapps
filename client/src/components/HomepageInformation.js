import React from "react";

// MaterialUI imports
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import ReactLogo from "../Logo.svg";
export default function HomepageInformation() {
  return (
    <Container style={{ marginTop: "2%", width: "100%" }}>
      <Grid container spacing={2}>
        <Grid item xs={3}></Grid>
        <Grid item xs={6}>
          <h2
            style={{
              color: "white",
              fontFamily: "monospace",
              fontWeight: "bold",
            }}
          >
            Obviously a fake lottery backed by{" "}
            <span style={{ color: "#18FF1E" }}>Lottery</span> admin
          </h2>
          <h2
            style={{
              color: "white",
              fontFamily: "monospace",
              fontWeight: "lighter",
            }}
          >
            Lottery contract
          </h2>
          <a href="https://en.wikipedia.org/wiki/Kryptonite">
            <img
              src={ReactLogo}
              alt="React Logo"
              style={{ height: "160px", width: "160px" }}
            />
          </a>
        </Grid>
        <Grid item xs={3}></Grid>
      </Grid>
    </Container>
  );
}
