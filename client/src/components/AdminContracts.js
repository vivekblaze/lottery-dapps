import React, { useState, useEffect } from "react";
// MaterialUI components
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import LocalOfferOutlinedIcon from "@material-ui/icons/LocalOfferOutlined";
import AccountCircleOutlinedIcon from "@material-ui/icons/AccountCircleOutlined";
import LocalAtmOutlinedIcon from "@material-ui/icons/LocalAtmOutlined";
import SupervisorAccountOutlinedIcon from "@material-ui/icons/SupervisorAccountOutlined";
import WorkIcon from "@material-ui/icons/Work";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

import Countdown from "react-countdown";
import MakeLottery from "./MakeLottery";

const STATUS = ["NotStarted", "Open", "Closed", "Completed"];

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: "3%",
    width: "40%",
  },
  rootList: {
    backgroundColor: theme.palette.background.paper,
    marginTop: 10,
    borderRadius: 15,
    borderColor: "#18FF1E",
    padding: 10,
  },
  root: {
    backgroundColor: "#20232a",
    border: "1px solid",
    borderRadius: 15,
    borderColor: "#18FF1E",
    padding: 10,
  },
  title: {
    color: "white",
    fontFamily: "monospace",
    fontWeight: "lighter",
  },
  button: {
    color: "white",
    fontSize: "15px",
    fontFamily: "monospace",
    fontWeight: "lighter",
    "&:hover": {
      backgroundColor: "#282c34",
    },
  },
  text: {
    color: "white",
    fontFamily: "monospace",
    fontWeight: "bold",
  },
  input: {
    color: "white",
  },
  dividerColor: {
    backgroundColor: "white",
  },
}));

export default function AdminContracts(props) {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [toastMessage, setToastMessage] = useState({
    message: "",
    status: "success",
  });
  const [informations, setInformations] = useState({
    lotteryID: "0",
    lotteryStatus: STATUS[0],
    costPerTicket: "0",
    drawMinFund: "0",
    lotteryDuration: "0",
    startingTimestamp: "0",
    endingTimestamp: "0",
    rewardPercentage: "0",
    lotteryWinner: [],
    playersCount: "0",
    currentBalance: "0",
    winnerReward: 0,
  });

  useEffect(() => {
    async function getLotteryInformations() {
      let getLotteryInfo = await props.contractContext.lotteryContract.methods
        .getLotteryInfo()
        .call();
      console.log("getLotteryInfo", getLotteryInfo);
      setInformations({
        lotteryID: getLotteryInfo.lotteryID,
        lotteryStatus: STATUS[getLotteryInfo.lotteryStatus],
        costPerTicket: getLotteryInfo.costPerTicket,
        drawMinFund: getLotteryInfo.drawMinFund,
        lotteryDuration: getLotteryInfo.lotteryDuration,
        startingTimestamp: getLotteryInfo.startingTimestamp,
        endingTimestamp: getLotteryInfo.endingTimestamp,
        rewardPercentage: getLotteryInfo.rewardPercentage,
        lotteryWinner: getLotteryInfo.lotteryWinner,
        playersCount: getLotteryInfo.playersCount,
        currentBalance: getLotteryInfo.currentBalance,
        winnerReward: parseFloat(
          (props.contractContext.web3.utils.fromWei(
            getLotteryInfo.currentBalance.toString(),
            "ether"
          ) *
            getLotteryInfo.rewardPercentage) /
            100
        ),
      });
      console.log("informations", informations);
    }
    props.contractContext && getLotteryInformations();
  }, [loading]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  async function handleLotteryCreation(value) {
    setLoading(true);
    try {
      await props.contractContext.lotteryContract.methods
        .createLottery(
          value.costPerTicket,
          value.drawMinFund,
          value.lotteryDuration,
          value.rewardPercentage
        )
        .send({
          from: props.contractContext.accounts[0],
        })
        .on("transactionHash", function (hash) {
          console.log("hash", hash);
        })
        .on("confirmation", function (confirmationNumber, receipt) {
          console.log("confirmationNumber", confirmationNumber);
        })
        .on("receipt", function (receipt) {
          console.log("receipt11111", receipt);
          setLoading(false);
          setToastMessage({
            message: "Lottery Created Successfully",
            status: "success",
          });
          setOpen(true);
        })
        .on("error", function (error, receipt) {
          console.log("error", error);
          setLoading(false);
          setToastMessage({
            message: error.message
              ? error.message
              : "Unable to create lottery now",
            status: "error",
          });
          setOpen(true);
        });
    } catch (error) {
      setLoading(false);
      setToastMessage({
        message: error.message,
        status: "error",
      });
      setOpen(true);
      console.log("error-caught", error);
    }
  }

  const Completionist = () => {
    const classes = useStyles();
    const handleDrawWinners = async () => {
      setLoading(true);
      try {
        await props.contractContext.lotteryContract.methods
          .draw()
          .send({
            from: props.contractContext.accounts[0],
          })
          .on("transactionHash", function (hash) {
            console.log("hash", hash);
          })
          .on("confirmation", function (confirmationNumber, receipt) {
            console.log("confirmationNumber", confirmationNumber);
          })
          .on("receipt", function (receipt) {
            console.log("receipt11111", receipt);
            setLoading(false);
            setToastMessage({
              message: "Winner founded successfully",
              status: "success",
            });
            setOpen(true);
          })
          .on("error", function (error, receipt) {
            console.log("error", error);
            setLoading(false);
            setToastMessage({
              message: error.message
                ? error.message
                : "Unable to find a winner",
              status: "error",
            });
            setOpen(true);
          });
      } catch (error) {
        setLoading(false);
        setToastMessage({
          message: error.message,
          status: "error",
        });
        setOpen(true);
      }
    };

    const handleRefundOrClose = async () => {
      setLoading(true);
      try {
        await props.contractContext.lotteryContract.methods
          .refund()
          .send({
            from: props.contractContext.accounts[0],
          })
          .on("transactionHash", function (hash) {
            console.log("hash", hash);
          })
          .on("confirmation", function (confirmationNumber, receipt) {
            console.log("confirmationNumber", confirmationNumber);
          })
          .on("receipt", function (receipt) {
            console.log("receipt11111", receipt);
            setLoading(false);
            setToastMessage({
              message: "Refunded successfully",
              status: "success",
            });
            setOpen(true);
          })
          .on("error", function (error, receipt) {
            console.log("error", error);
            setLoading(false);
            setToastMessage({
              message: error.message ? error.message : "Refund failed",
              status: "error",
            });
            setOpen(true);
          });
      } catch (error) {
        setLoading(false);
        setToastMessage({
          message: error.message,
          status: "error",
        });
        setOpen(true);
      }
    };
    return (
      <div>
        <Grid container spacing={2}>
          {informations.playersCount > 0 && (
            <Grid item xs={12}>
              <Button
                className={classes.button}
                variant="outlined"
                onClick={handleDrawWinners}
              >
                Draw winners
              </Button>
            </Grid>
          )}
          <Grid item xs={12}>
            <Button
              className={classes.button}
              variant="outlined"
              onClick={handleRefundOrClose}
            >
              {informations.playersCount == 0
                ? "Close lottery"
                : "Refund to all"}
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  };

  const Informations = () => {
    const classes = useStyles();
    return (
      <List className={classes.rootList}>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <LocalOfferOutlinedIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Cost per ticket"
            secondary={
              props.contractContext.web3.utils.fromWei(
                informations.costPerTicket,
                "ether"
              ) + " ETH"
            }
          />
        </ListItem>
        <Divider variant="inset" component="li" />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <WorkIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Minimum draw fund"
            secondary={informations.drawMinFund + " ETH"}
          />
        </ListItem>
        <Divider variant="inset" component="li" />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <AccountCircleOutlinedIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Total Players"
            secondary={informations.playersCount}
          />
        </ListItem>

        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <LocalAtmOutlinedIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Winner Reward"
            secondary={
              Number(informations.winnerReward) > 0
                ? informations.winnerReward + " ETH"
                : 0 + " ETH"
            }
          />
        </ListItem>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <SupervisorAccountOutlinedIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Owner Commission"
            secondary={
              props.contractContext.web3.utils.fromWei(
                (
                  informations.currentBalance - informations.winnerReward
                ).toString(),
                "ether"
              ) + " ETH"
            }
          />
        </ListItem>
      </List>
    );
  };

  const renderer = ({ days, hours, minutes, seconds, completed }) => {
    console.log("completed", completed);
    if (completed) {
      return <Completionist />;
    } else {
      return (
        <div>
          <div className="countdown-wrapper">
            <div className="time-section">
              <div className="countdown-value">{days}</div>
              <div className="countdown-unit">Days</div>
            </div>
            <div className="time-section">
              <div className="countdown-value">{hours}</div>
              <div className="countdown-unit">Hours</div>
            </div>
            <div className="time-section">
              <div className="countdown-value">{minutes}</div>
              <div className="countdown-unit">Mins</div>
            </div>
            <div className="time-section">
              <div className="countdown-value">{seconds}</div>
              <div className="countdown-unit">Secs</div>
            </div>
          </div>
          <Informations />
        </div>
      );
    }
  };

  return (
    <Container className={classes.container}>
      <Paper className={classes.root} elevation={3}>
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
          <Alert onClose={handleClose} severity={toastMessage.status}>
            {toastMessage.message}
          </Alert>
        </Snackbar>

        <Grid container spacing={2}>
          <Grid item xs={12}>
            <h1 className={classes.title}>Lottery Dashboard</h1>
          </Grid>
          {loading ? (
            <Grid item xs={12}>
              <CircularProgress color="secondary" />
            </Grid>
          ) : (
            <>
              {informations.lotteryStatus == "Open" ? (
                <Grid item xs={12}>
                  <Countdown
                    date={new Date(informations.endingTimestamp * 1000)}
                    renderer={renderer}
                  />
                </Grid>
              ) : (
                <Grid item xs={12}>
                  <MakeLottery createLottry={handleLotteryCreation} />
                </Grid>
              )}
            </>
          )}
        </Grid>
      </Paper>
    </Container>
  );
}
