import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

import { Formik } from "formik";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: "3%",
    width: "40%",
  },
  rootList: {
    backgroundColor: theme.palette.background.paper,
    marginTop: 10,
    borderRadius: 15,
    borderColor: "#18FF1E",
    padding: 10,
  },
  root: {
    backgroundColor: "#20232a",
    border: "1px solid",
    borderRadius: 15,
    borderColor: "#18FF1E",
    padding: 10,
  },
  title: {
    color: "white",
    fontFamily: "monospace",
    fontWeight: "lighter",
  },
  button: {
    color: "white",
    fontSize: "15px",
    fontFamily: "monospace",
    fontWeight: "lighter",
    "&:hover": {
      backgroundColor: "#282c34",
    },
  },
  text: {
    color: "white",
    fontFamily: "monospace",
    fontWeight: "bold",
  },
  input: {
    color: "white",
  },
  dividerColor: {
    backgroundColor: "white",
  },
}));

const validationSchema = Yup.object({
  costPerTicket: Yup.string("Enter the price of ticket").required(
    "Price is required"
  ),
  drawMinFund: Yup.string("Enter the maximum of ticket limit").required(
    "Maximum price is required"
  ),
  lotteryDuration: Yup.string("Enter the lotteryDuration percentage").required(
    "Duration is required"
  ),
  rewardPercentage: Yup.string("Enter the reward percentage").required(
    "Reward Percentage is required"
  ),
});

export default function MakeLottery(props) {
  const classes = useStyles();
  const [information, setInformation] = useState({
    costPerTicket: "",
    drawMinFund: "",
    lotteryDuration: "",
    rewardPercentage: "",
  });

  const handleFormSubmit = (data) => {
    console.log("Settings data", data);
    setInformation({
      costPerTicket: data.costPerTicket,
      drawMinFund: data.drawMinFund,
      lotteryDuration: data.lotteryDuration,
      rewardPercentage: data.rewardPercentage,
    });
    props.createLottry(data);
  };
  return (
    <div>
      <Paper className={classes.rootList}>
        <Formik
          initialValues={{ ...information }}
          validationSchema={validationSchema}
          onSubmit={handleFormSubmit}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setSubmitting,
            setFieldValue,
            isValid,
          }) => (
            <form onSubmit={handleSubmit}>
              <TextField
                name="costPerTicket"
                // helperText={touched.costPerTicket ? errors.costPerTicket : ""}
                error={Boolean(errors.costPerTicket)}
                label="Ticket Price"
                value={values.costPerTicket}
                onChange={handleChange}
              />
              <div style={{ color: "red", fontSize: "12px" }}>
                {Boolean(errors.costPerTicket) ? errors.costPerTicket : ""}
              </div>
              <TextField
                name="drawMinFund"
                // helperText={touched.drawMinFund ? errors.drawMinFund : ""}
                error={Boolean(errors.drawMinFund)}
                label="Minimum Fund"
                value={values.drawMinFund}
                onChange={handleChange}
              />
              <div style={{ color: "red", fontSize: "12px" }}>
                {Boolean(errors.drawMinFund) ? errors.drawMinFund : ""}
              </div>
              <TextField
                name="lotteryDuration"
                // helperText={touched.lotteryDuration ? errors.lotteryDuration : ""}
                error={Boolean(errors.lotteryDuration)}
                label="Lottery Duration"
                value={values.lotteryDuration}
                onChange={handleChange}
              />
              <div style={{ color: "red", fontSize: "12px" }}>
                {Boolean(errors.lotteryDuration) ? errors.lotteryDuration : ""}
              </div>
              <TextField
                name="rewardPercentage"
                // helperText={touched.rewardPercentage ? errors.rewardPercentage : ""}
                error={Boolean(errors.rewardPercentage)}
                label="Reward Percentage"
                value={values.rewardPercentage}
                onChange={handleChange}
              />
              <div style={{ color: "red", fontSize: "12px" }}>
                {Boolean(errors.rewardPercentage)
                  ? errors.rewardPercentage
                  : ""}
              </div>
              <Button
                style={{ marginTop: 10 }}
                type="submit"
                variant="outlined"
                color="default"
                disabled={!isValid}
              >
                Create lottery
              </Button>
            </form>
          )}
        </Formik>
      </Paper>
    </div>
  );
}
